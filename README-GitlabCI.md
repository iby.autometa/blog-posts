<!--

This file is a template to help you get started writing a "conceptual" article.
Read our writing guidelines for more information on how to write articles for
the Rancher community:

    https://rancher.com/writing-program/writing-guidelines

-->
<p>
---
<br>
title: "Deploy Rancher cluster using GitLab CI and Terraform"
<br>
author: Idan Ben-Yair
<br>
date: 5/11/2020
<br>
description: "The following article dives deeper into the use of CI/CD tools with IaC languages such as Terrafrom. In conjunction with the Rancher2 provider."
<br>
type: "blog"
<br>
tags: ["Conceptual"]
<br>
categories: [blog]
<br>
image: ""
<br>
draft: true
<br>
---
</p>
<!-- In the front matter above, fill out the title, author, and description
fields. -->

## Introduction

In todays ever changing world of DevOps it is important to follow best practices. That goes for security, access control, resource limits etc. One of the most important things in the world of DevOps is CI/CD. Continuous integration is a crucial part of an efficient deployment. We are all guilty of repeating manual steps over and over again. Especially when it comes to node configuration. The "automate all things" mindset is required to keep us on track so we could perform as efficiently as possible. And that our applications will deploy and work as efficiently as possible. Through the GitLab CI/CD options you get a user friendly UI that allows you to configure your build and customize it as you see fit. Setting Pipeline triggers, build variables, license compliance and more.

The ability to view your build steps from one console is very helpful. Especially when attempting to troubleshoot builds.
The CLI output of the running commands is also displayed on each build step. That way you have a perspective of whats going on in the build. Without actually SSHing into the runner node. CI/CD tools usually function with a build file. The build file will determine the build steps. In this case. While utilizing Gitlab CI/CD the build file is called `.gitlab-ci.yaml`. In this article you will get a glimps into how this build file is put together and what it actually does.

Another important part in this deployment is how the GitLab CI tool communicates with AWS in order to trigger the launch of new resources. This deployment also consists of Terraform, RKE and Rancher2 provider. The main objective is to produce a pipeline that will deploy and destroy infrastructure on demand. The end result is highly available, consistent deployment that we could trigger with a click of a button. Or with a single CLI command (or two).



<!-- Include paragraphs describing article scope, why it's helpful, who should
read it, and what the reader will learn. -->

## What does the deployment look like?

In this deployment model there are a few moving parts that we need to keep in mind. Each component in this deployment has a specific purpose.
The goal of the deployment is to deploy the minimal amount of resource that is required. Following best practices of security, cost and high availability.

Conceptually a CI/CD pipeline should have 3 stages; source, build, deploy:

* **Source** - In every deployment a source control tool is being used. Most of the time GitHub or GitLab will be used. Bitbucket is also a good option. 
           The reason GitLab was chosen for this scenario is due to the fact that it also offers a built in CI/CD functionality.
           So GitLab is not only our source control tool that allows us to manage commits, and different branches of our code.

* **Build**  - The stages mentioned in the `.gitlab-ci.yaml` will define the build steps. In this phase the GitLab platform will `validate` the code and run a `terraform plan`.
           Within the stages it is possible to pass commands, set variables, build docker images, create files etc. This allows us to decouple the steps.
           That means that it will be easier to now remove stages and add new ones if we choose to.  

* **Deploy** - In this phase there are two manual actions. These two options were defined as a manual step in the build yaml file. 
           The 1st action we can take is `deploy`. This option will initiate the creation of the infrastructure utilizing the Terraform code.
           Once this manual step is executed GitLab will reachout to AWS. Authenticate with an access and secret key. And start deploying the infrastructure into the public cloud.
           In this case AWS. Another component that plays a major part is the `provider.tf` file. This file defines the cloud provider for the deployment.
           The 2nd option we have is `destroy`. Just like `deploy` is triggered manually. In some scenarios it is plausible to automate this step.
           But in most cases we really want to be careful when either executing a deployment or destroying it. It is also recommeneded to limit the access to execute these stages.
           Using a user database and applying permissioms for the execution of these manual steps will be security best practices.

### Infrastructure Diagram

This diagram presents all the different tools that this deployment is using:

* **GitLab** - Source control and CI/CD.
* **Amazon Web Services** - EC2, S3, R53, Security groups, ELB.
    * S3 - The S3 bucket is the only component in this deployment that has to be created manually.
           Before you start your deployment, make sure you create your bucket and specify it in the variables section.
           The S3 bucket will maintain the `terraform.tfstate` file. More about managing Terraform state here: [Terraform State](https://www.terraform.io/docs/state/index.html) 
* **Terraform** - Infrastructure as code.
    * RKE Provider - Allows for K8s cluster provisioning.
    * Rancher2 Provider - Allows for Rancher management cluster configuration from the terraform code.
    * helm provider - Allows the installation of helm charts and ultimately the installation of Rancher on the created infra.
<br>
  <img align="center" width="100%" src="img/gitlab-rancher.png">

<!-- OPTIONAL! -->

<!-- You can optionally include a what is section to introduce and broadly
define the theme of the article. -->

## How does the pipeline work?

<!-- OPTIONAL! -->

<!-- Often, it is useful to include a section defining any specialized
terminology that will be used in the article or that the reader might come
across while doing independent research. -->

### Stages
```
stages:
  - validate
  - plan_before_apply
  - apply
  - destroy
```

### Before Script
The before script lays the building blocks for this deployment to succeed. 
Two files are created in this process:

1. backend.ft - This file will be responsible for the s3 bucket that holds the ftstate.
```
  - |
    cat <<EOF > backend.tf
        terraform {
           backend "s3" {
             bucket     = "$BUCKET_NAME"
             key        = "$BUCKET_KEY"
             region     = "us-east-1"
             encrypt    = true
          }
        }
    EOF

```
2. variables.tf - This file will hold the credentials, VPC, K8s version etc. These parameters are passed from the `settings` area of the GitLab dashboard.

```
    cat <<EOF > variables.tf
        variable "aws_access_keys" {
        type = map(string)
        description = "AWS Access Keys for terraform deployment"

        default = {
            access_key = "$AWS_ACCESS_KEY_ID"
            secret_key = "$AWS_SECRET_ACCESS_KEY"
            region = "us-east-1"
        }
        }
        variable "number_of_nodes" {
        type = string
        description = "Number of nondes"
        default = "$NUMBER_OF_NODES"
        }

...
```
### Build stages
1. **Validate** - Will validate the configuration files in the working directory.
```
validate:
  stage: validate
  script:
    - terraform validate
```
2. **plan_before_apply** - Will run terraform plan and create an execution plan.
```
plan_before_apply:
  stage: plan_before_apply
  script:
    - terraform plan
  dependencies:
    - validate
```
3. **Apply** - Will run terraform apply and execute the plan. This is a manual step.
```
apply:
  stage: apply
  script:
    - apk update && apk add curl git
    - curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
    - chmod u+x kubectl && mv kubectl /bin/kubectl
    - mkdir -p ~/.kube
    - echo '' >  ~/.kube/config
    - apk add --update --no-cache curl ca-certificates
    - curl -L https://get.helm.sh/helm-v3.1.2-linux-amd64.tar.gz |tar xvz
    - mv linux-amd64/helm /usr/bin/helm
    - chmod +x /usr/bin/helm
    - terraform apply --auto-approve
  dependencies:
    - plan_before_apply
  when: manual
```
4. **Destroy** - Will destroy all resources created in the apply stage. This is a manual step as well.
```
destroy:
  stage: destroy
  script:
    - mkdir -p ~/.kube
    - echo '' >  ~/.kube/config  
    - terraform state rm "helm_release.cert_manager"
    - terraform state rm "helm_release.rancher"
    - terraform destroy --auto-approve
  dependencies:
    - apply
  when: manual
```

### Apply
In order to execute `terraform apply` navigate to the `CI/CD` section of your project. Click on `New Pipeline`. And run a new pipeline.
Once the `validate` and `plan` stages have been completed. Click on the `apply` step and run. You should be aware of commits to the repo.

### Destroy
To destroy the deployment click on the `destroy` step in the CD/CD console and run. Terraform will destroy all the infra previously created by the pipeline.
The only thing that will be left behind is the s3 bucket that container the terraform.tfstate. The terraform state is crucial if a destroy step needs to be executed.

### Variables
In order to set the number of nodes for the cluster, Kubernetes version, Rancher version etc. You will need to navigate to the `Settings` page of the project and under `CI/CD` you can set yuor variables.

### Suggested values for environment variables:
| Var Name               | Var Value                | Var Type |
| :------:               |  :------:                | :------: |
| VPC                    | Your AWS VPC             | String   |
| AWS_ACCESS_KEY_ID      | Your AWS Access Key      | String   |
| AWS_SECRET_ACCESS_KEY  | Your AWS Secret Key      | String   |
| DOMAIN                 | Your domain name         | String   |
| EMAIL                  | Email of maintainer      | String   |
| INSTANCE_TYPE          | AWS EC2 Instance Type    | String   |
| K8S_VERSION            | Latest K8s version       | String   |
| NUMBER_OF_NODES        | 3                        | String   |
| RANCHER_VERSION        | Latest Rancher version   | String   |
| UI_PASSWORD            | Password for Rancehr UI  | String   |
| BUCKET_NAME            | Name of S3 bucket        | String   |
| BUCKET_KEY             | Path to tfstate file     | String   |


### AWS Cloud provider
The AWS Cloud provider is used in this deployment. More information about the provider and how it works refer to this documentation: [AWS Provider](https://www.terraform.io/docs/providers/aws/index.html)

A good example of how to use the provider could be found in the `provider.tf` file. This provider file will allow the Terraform code to interact with AWS and deploy resources (EC2, Security Groups, Load Balancer etc).

```
provider "aws" {
  region  = "us-east-1"
  profile = "default"
  access_key      = lookup(var.aws_access_keys, "access_key")
  secret_key      = lookup(var.aws_access_keys, "secret_key")

}
```

### Rancher2 provider

The Rancher2 provider is a Terraform component and needs to be imported as a plugin in order to work. A good example of how the provider is used can be found in `rancher-ha.tf`.

```
resource "rancher2_bootstrap" "admin" {
  provider = rancher2.bootstrap
  depends_on = [null_resource.wait_for_rancher]
  password = var.ui_password
}
```
The Rancher2 provider is being used in order to created the Rancher UI Admin account. More about the Rancher2 provider: [Rancher2 Provider](https://www.terraform.io/docs/providers/rancher2/index.html)

### GitLab Runner
If you do not have a runner node configured you can use this repo to set the runner up with the proper configuration: [gitlab-runner-aws](https://gitlab.com/iby.rancher/gitlab-runner-aws). Or follow the instruction in gitlab docs to setup a new runner: [GitLab Runner Installation](https://docs.gitlab.com/runner/install/).

If you already have a runner active. You can simply add this configuration:

```
#Register the runner
sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "<Your project token>" \
  --executor "docker" \
  --docker-image hashicorp/terraform \
  --description "docker-runner" \
  --tag-list "" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"
```
<!-- After the introductory sections, include sections that introduce and
explain the concepts, processes, and systems related to the topic.  The section
names and organization will vary widely from article to article.

## (Section)

### sub-section
### sub-section

## (Section)

### sub-section
### sub-section

## (Section)

-->

## Conclusion
There are a few important take aways from this blogpost. We must always think with automation first mindset. 
Working with CI/CD tools will get rid of the overhead of managing infra manually. CI/CD tools allow us to collaborate more efficiently.
CI/CD tools give us a deep insight in regards to the build steps and the CLI output from the runner nodes.

Overall, using CI/CD helps us follow best practices in code integration, code build and code deployment stages.
Along with IaC tools such as Terraform and the cloud providers for AWS, Azure and GCP. Will allow you to deploy your code along with your infra easily.
<!--

A brief wrap up describing the topic covered and linking to additional
conceptual or practical articles that the reader can use to further their
understanding or begin implementation.

-->
