<!--

This file is a template to help you get started writing a "tutorial" article.
Read our writing guidelines for more information on how to write articles for
the Rancher community:

    https://rancher.com/writing-program/writing-guidelines

-->
---
<p>
title: Longhorn local storage provider with K3s
<br>
author: Idan Ben Yair
<br>
date: 5/7/2020
<br>
description: This article is a tutorial for setting up a local persistent storage solution. Provisioning will be done through the Longhorn UI using the local storage provider. K3s will be used in this scenario. A Raspberry Pi cluster will be used for the infrastructure. Further details on the cluster setup will be covered in this article.
<br>
type: "blog"
<br>
tags: ["Tutorial"]
<br>
categories: [blog]
<br>
image: ""
</p>
draft: true
---

<!-- In the front matter above, fill out the title, author, and description
fields. -->

## Introduction

### What will be covered?
This tutorial will cover persistent storage configuration for ARM devices. A Raspberry Pi cluster will be used to illustrate the functionality of Longhorn's local storage provider. Using the local storage provider we can configured an attached HDD (2.5 500gb Hard Drive) as our persistent storage solution. The Pi's will be configured with their own 32gb SD cards in order to run the OS and workloads. By using a PV (Persistent Volume) and a PVC (Persistent Volume Claim) we will utilize the 500gb hardrive to store either application data or logs. Or maybe even both.

### How will this be useful to you?
This is helpful in many ways. Its important to remember that K3s just like K8s is built for speed and performance. So if we do want to retain some of the important workload data or log streams. We can store it in a persistent solution. That way major data loss could be avoided.

### Target Audience
The main audience for this article will be dev and ops people that want to know more about Longhorn and persistent storage in general. It is highly important to understand storage since that is something that will be managed by the user. Not by K3s or K8s like other components. This is article might be useful for people that want to experiment with ARM devices and do some testing in a k3s environment.

### Takeaways
The reader should gain basic knoweldge on how to configure a local storage provider using Longhorn. This could also be implemented on a K8s infra as well. 
<!-- Include paragraphs describing article scope, why it's helpful, who should
read it, and what the reader will learn. -->

## Background

<!-- OPTIONAL! -->

<!-- If you are writing about technology too complex to cover adequately in the
introduction, you can optionally include a background section to provide
additional context. -->

## Prerequisites

### Hardware
First we will need the physical hardware. This is not a tutorial on how to setup the K3s Pi cluster. There is no need to dive deep on every step of the setup since its already been covered in a different article. You can view the step by step guide in this link: [Kubernetes Cluster On Raspberry Pi](https://www.linxlabs.com/kubernetes-cluster-on-raspberry-pi-4-part-1/)

The above article was used as reference. In this scenario different and/or extra parts will be used.

### List of parts used in this scenario
| Part                       | Link To Product                                                                                                |
| ---                        |  :------:                                                                                                      | 
| Case                       | [Cloudlet CASE](https://www.amazon.com/gp/product/B07D5NM9ZG/ref=ppx_yo_dt_b_asin_title_o09_s00?ie=UTF8&psc=1) |
| 4 X Heatsink set           | [Cloudlet CASE](https://www.amazon.com/gp/product/B07D5NM9ZG/ref=ppx_yo_dt_b_asin_title_o09_s00?ie=UTF8&psc=1) |
| 4 X 32gb sandisk SD        | [Cloudlet CASE](https://www.amazon.com/gp/product/B07D5NM9ZG/ref=ppx_yo_dt_b_asin_title_o09_s00?ie=UTF8&psc=1) | 
| 4 X Raspberry Pi 4 Model B | [Cloudlet CASE](https://www.amazon.com/gp/product/B07D5NM9ZG/ref=ppx_yo_dt_b_asin_title_o09_s00?ie=UTF8&psc=1) |
| 2.5 500gb HDD              | [Cloudlet CASE](https://www.amazon.com/gp/product/B07D5NM9ZG/ref=ppx_yo_dt_b_asin_title_o09_s00?ie=UTF8&psc=1) | 
| 8 port switch              | [Cloudlet CASE](https://www.amazon.com/gp/product/B07D5NM9ZG/ref=ppx_yo_dt_b_asin_title_o09_s00?ie=UTF8&psc=1) |
| 4 X Cat6 cables            | [Cloudlet CASE](https://www.amazon.com/gp/product/B07D5NM9ZG/ref=ppx_yo_dt_b_asin_title_o09_s00?ie=UTF8&psc=1) |
| Power Supply               | [Cloudlet CASE](https://www.amazon.com/gp/product/B07D5NM9ZG/ref=ppx_yo_dt_b_asin_title_o09_s00?ie=UTF8&psc=1) |
| USB A to C cables          | [Cloudlet CASE](https://www.amazon.com/gp/product/B07D5NM9ZG/ref=ppx_yo_dt_b_asin_title_o09_s00?ie=UTF8&psc=1) |

### Software

For this scenario we will need to make sure our Pis are flashed with Raspbian Buster Lite images. These are the tools you will need to in order to flash the SD cards for each pi.

  - [Raspbian Buster Lite](https://www.raspberrypi.org/downloads/raspbian/) - The image that is needed for the OS of the Pis. It is also possible to use Ubuntu (other OSs could also work. But ubuntu and Raspbian are known to work well in this scenario).
  - [BalenaEtcher](https://www.balena.io/etcher/) - This software will allow you to flash the SD cards.
  - [Angry IP Scanner](https://angryip.org/download/) - This will help you discover the Pis on your network.

After the SD cards are configured with the OS connect them to the Pis. Once thats done you can move forward and configure SSH and DHCP for your nodes. This is also covered in the article above.
<!-- 

A list of technology requirements that the user will need to follow along.
Each prerequisite should include a description and a link to the user can
follow to fulfill the dependency.

The general pattern should look like this:

    In order to complete this guide, you will need the following:

    * **requirement:** requirement description with [requirement link]()
    * **requirement:** requirement description with [requirement link]()
    * **requirement:** requirement description with [requirement link]()

-->

## Goals
Next, we will need to install Longhorn either with Helm or Kubectl. Follow these steps to install Longhorn on your K3s cluster based on the method you would like to use.
  
### Install Longhorn using Helm 3
Install Helm 3
```
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
```
Export env variable in order to communicate with the cluster
```
export KUBECONFIG=/etc/rancher/k3s/k3s.yaml
```
Clone Longhorn repo
```
git clone https://github.com/longhorn/longhorn
```
Create namespace and install longhorn on namespace
```
kubectl create namespace longhorn-system
helm install longhorn ./longhorn/chart/ --namespace longhorn-system
```
Check that pods are running
```
kubectl -n longhorn-system get pod
```
Get external URL from the Longhorn service and access UI
```
kubectl -n longhorn-system get svc
```
### Install Longhorn using kubectl
Export env variable in order to communicate with the cluster
```
export KUBECONFIG=/etc/rancher/k3s/k3s.yaml
```
Apply longhorn.yaml definition file to install Longhorn onto the <b>longhorn-system</b> namespace
```
kubectl apply -f https://raw.githubusercontent.com/longhorn/longhorn/master/deploy/longhorn.yaml
```
Create storage class
```
kubectl create -f https://raw.githubusercontent.com/longhorn/longhorn/master/examples/storageclass.yaml
```
### Setup HDD as a PV from the Longhorn UI

### Use the PV by creating PVC and attach to a workload

<!-- OPTIONAL -->

<!-- For complex articles, include a goals section to outline the desired end
state. -->




<!-- After the introductory sections, include sections outlining individual
steps the reader can follow to complete the procedure.  Divide these up into
subsections as needed to improve clarity.

## (step one)

### (sub-step) ### (sub-step)

## (step two)

### (sub-step) ### (sub-step)

## (step ...)

-->

## Conclusion

<!--

A brief wrap up describing the final state the reader should be in as well as
links to any additional resources or next steps.

-->
